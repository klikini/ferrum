use rand::RngCore;
use rand::rngs::StdRng;

use crate::canvas::Canvas;
use crate::color::Color;
use crate::drawable::Drawable;
use crate::util::{abs_diff, rand_max, rand_range};

pub(crate) struct Gradient {
    start_color: Color,
    stop_color: Color,
    is_radial: bool,
    radial_center: (u32, u32),
    linear_length: u32,
}

impl Gradient {
    pub fn sky(rng: &mut StdRng, canvas: &Canvas) -> Self {
        let is_radial = rng.next_u32() % 2 == 0;

        let center = if is_radial {
            (
                ((rand_max(rng, canvas.width) as f32) / (rand_range(rng, 1, 5) as f32)) as u32,
                canvas.height - (((rand_max(rng, canvas.height) as f32) / (rand_range(rng, 1, 5) as f32)) as u32)
            )
        } else {
            (0, 0)
        };

        Self {
            start_color: Color::from(rng.next_u32()),
            stop_color: Color::from(rng.next_u32()),
            is_radial,
            radial_center: center,
            linear_length: canvas.height,
        }
    }

    pub fn clouds(rng: &mut StdRng, canvas: &Canvas) -> Self {
        let color = Color::grey((rng.next_u32() % 128) as u8);

        Self {
            start_color: color.clone(),
            stop_color: color.opacity(0),
            is_radial: false,
            radial_center: (0, 0),
            linear_length: (rng.next_u32() % canvas.height),
        }
    }

    fn paint_radial(&self, canvas: &mut Canvas) {
        eprintln!("Painting radial gradient {} -> {}", self.start_color, self.stop_color);

        let diagonal = (self.radial_center.0 as f32).hypot(self.radial_center.1 as f32);

        for x in 0..canvas.width {
            for y in 0..canvas.height {
                let dist = (abs_diff(self.radial_center.0, x) as f32).hypot(abs_diff(self.radial_center.1, y) as f32);
                let color = self.start_color.linear_interpolate(&self.stop_color, &(dist / diagonal));
                canvas.set_px(x, y, &color);
            }
        }
    }

    fn paint_linear(&self, canvas: &mut Canvas) {
        eprintln!("Painting linear gradient {} -> {} x {}", self.start_color, self.stop_color, self.linear_length);

        for line in 0..self.linear_length {
            let progress = line as f32 / self.linear_length as f32;
            let color = self.start_color.linear_interpolate(&self.stop_color, &progress);

            for px in 0..canvas.width {
                canvas.set_px(px, line, &color);
            }
        }
    }
}

impl Drawable for Gradient {
    fn paint(&self, canvas: &mut Canvas) {
        if self.is_radial {
            self.paint_radial(canvas);
        } else {
            self.paint_linear(canvas);
        }
    }
}
