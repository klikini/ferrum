use rand::RngCore;
use rand::rngs::StdRng;

use crate::canvas::Canvas;
use crate::drawable::Drawable;

pub(crate) struct Grain {
    shifts: Vec<u8>,
}

impl Grain {
    pub(crate) fn generate(rng: &mut StdRng, canvas: &Canvas) -> Self {
        let mut shifts: Vec<u8> = vec![];
        shifts.reserve((canvas.width * canvas.height) as usize);

        for _ in 0..canvas.width * canvas.height {
            shifts.push((rng.next_u32() % 10) as u8);
        }

        Self { shifts }
    }
}

impl Drawable for Grain {
    fn paint(&self, canvas: &mut Canvas) {
        eprintln!("Painting grain");

        for p in 0..(canvas.width * canvas.height) as usize {
            let color = canvas.get_idx(p).darken(self.shifts[p]);
            canvas.set_idx(p, &color);
        }
    }
}
