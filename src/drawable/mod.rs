use crate::canvas::Canvas;

pub mod gradient;
pub mod terrain;
pub(crate) mod grain;

pub trait Drawable {
    fn paint(&self, canvas: &mut Canvas);
}
