use rand::prelude::StdRng;
use rand::RngCore;

use crate::canvas::Canvas;
use crate::color::Color;
use crate::drawable::Drawable;
use crate::util::rand_range;

pub(crate) struct Terrain {
    hills: Vec<Hill>,
}

struct Hill {
    color: Color,
    offset: i32,
    slope: f32,
}

impl Terrain {
    pub fn generate(rng: &mut StdRng, canvas: &Canvas) -> Terrain {
        let mut hills: Vec<Hill> = vec![];

        for _ in 0..rand_range(rng, 1, 5) {
            hills.push(Hill::generate(rng, canvas));
        }

        Terrain {
            hills,
        }
    }
}

impl Drawable for Terrain {
    fn paint(&self, canvas: &mut Canvas) {
        for hill in &self.hills {
            hill.paint(canvas);
        }
    }
}

impl Hill {
    pub fn generate(rng: &mut StdRng, canvas: &Canvas) -> Hill {
        // Between 1/2 and 1/6 of canvas will be sky at each end
        let height_start = (canvas.height / rand_range(rng, 2, 5)) as i32;
        let height_end = (canvas.height / rand_range(rng, 2, 5)) as i32;

        // Change in height over change in x (canvas width)
        let slope = (height_end - height_start) as f32 / (canvas.width as f32);

        Hill {
            color: Color::from(rng.next_u32() & rng.next_u32()),
            slope,
            offset: height_start,
        }
    }
}

impl Drawable for Hill {
    fn paint(&self, canvas: &mut Canvas) {
        eprintln!("Painting hill {}", self.color);

        for x in 0..canvas.width {
            let calculated = self.offset + ((self.slope * (x as f32)) as i32);
            let max_y = (canvas.height as i32 - calculated) as u32;

            let edge_color = self.color.opacity(127);
            canvas.set_px(x, max_y, &edge_color);

            for y in max_y + 1..canvas.height {
                canvas.set_px(x, y as u32, &self.color);
            }
        }
    }
}
