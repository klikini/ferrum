use std::fmt::{Display, Formatter};
use std::fmt;

pub(crate) struct Color {
    pub(crate) r: u8,
    pub(crate) g: u8,
    pub(crate) b: u8,
    pub(crate) a: u8,
}

impl Color {
    /// Create a greyscale color with the given value and full opacity
    pub fn grey(value: u8) -> Self {
        Self { r: value, g: value, b: value, a: 255 }
    }

    /// Create a copy of this color with an alpha value
    pub fn opacity(&self, alpha: u8) -> Self {
        Self {
            r: self.r,
            g: self.g,
            b: self.b,
            a: alpha,
        }
    }

    /// Blend other on top of this color
    pub fn blend(&self, other: &Self) -> Self {
        if other.a == 255 {
            // Completely covers this color
            other.clone()
        } else if self.a == 255 {
            let a_fac = other.a as f32 / 255f32;
            let inv_a = 1f32 - a_fac;

            Self {
                r: (a_fac * other.r as f32 + inv_a * self.r as f32) as u8,
                g: (a_fac * other.g as f32 + inv_a * self.g as f32) as u8,
                b: (a_fac * other.b as f32 + inv_a * self.b as f32) as u8,
                a: 255u8,
            }
        } else {
            panic!("Cannot blend two colors with alpha channels");
        }
    }

    /// Dilute this color by a progress factor (0 = this color, 1 = other color)
    pub fn linear_interpolate(&self, other: &Color, progress: &f32) -> Self {
        Self {
            r: ((other.r as i16 - self.r as i16) as f32 * progress + self.r as f32) as u8,
            g: ((other.g as i16 - self.g as i16) as f32 * progress + self.g as f32) as u8,
            b: ((other.b as i16 - self.b as i16) as f32 * progress + self.b as f32) as u8,
            a: ((other.a as i16 - self.a as i16) as f32 * progress + self.a as f32) as u8,
        }
    }

    pub fn darken(&self, amount: u8) -> Self {
        Self {
            r: self.r.saturating_sub(amount),
            g: self.g.saturating_sub(amount),
            b: self.b.saturating_sub(amount),
            a: self.a,
        }
    }
}

impl Display for Color {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if self.a == 255 {
            write!(f, "rgb({}, {}, {})", self.r, self.g, self.b)
        } else {
            write!(f, "rgba({}, {}, {}, {})", self.r, self.g, self.b, self.a)
        }
    }
}

impl Clone for Color {
    fn clone(&self) -> Self {
        Color {
            r: self.r,
            g: self.g,
            b: self.b,
            a: self.a,
        }
    }

    fn clone_from(&mut self, source: &Self) {
        self.r = source.r;
        self.g = source.g;
        self.b = source.b;
        self.a = source.a;
    }
}

/// Create a greyscale color with full opacity from a value
impl From<u8> for Color {
    fn from(v: u8) -> Self {
        Self {
            r: v,
            g: v,
            b: v,
            a: 255u8,
        }
    }
}

/// Decode an integer into a color with full opacity
impl From<u32> for Color {
    fn from(packed: u32) -> Self {
        Self {
            r: (packed & 255u32) as u8,
            g: ((packed >> 8) & 255u32) as u8,
            b: ((packed >> 16) & 255u32) as u8,
            a: 255u8,
        }
    }
}

/// Store an RGB tuple as a color with full opacity
impl From<(u8, u8, u8)> for Color {
    fn from(rgb: (u8, u8, u8)) -> Self {
        return Self {
            r: rgb.0,
            g: rgb.1,
            b: rgb.2,
            a: 255u8,
        };
    }
}

/// Store an RGBA tuple as a color
impl From<(u8, u8, u8, u8)> for Color {
    fn from(rgba: (u8, u8, u8, u8)) -> Self {
        return Color {
            r: rgba.0,
            g: rgba.1,
            b: rgba.2,
            a: rgba.3,
        };
    }
}
