use rand::RngCore;
use rand::rngs::StdRng;

pub fn rand_max(rng: &mut StdRng, max: u32) -> u32 {
    rng.next_u32() % max
}

pub fn rand_range(rng: &mut StdRng, min: u32, max: u32) -> u32 {
    (rng.next_u32() % max) + min
}

pub fn abs_diff(a: u32, b: u32) -> u32 {
    if a > b {
        a - b
    } else {
        b - a
    }
}
