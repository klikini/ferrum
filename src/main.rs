use std::fs::File;
use std::io::{stdout, Write};
use std::path::Path;
use std::process::exit;
use std::time::{SystemTime, UNIX_EPOCH};

use clap::clap_app;
use rand::rngs::StdRng;
use rand::SeedableRng;

mod canvas;
mod color;
mod drawable;
mod util;

fn main() {
    let args = clap_app!(ferrum =>
        (version: "0.1.0")
        (author: "Andy Castille <me@andycc.dev>")
        (about: "Procedurally generates nature scenes")
        (@arg width: --width -w +takes_value default_value("1024") "Sets output image width")
        (@arg height: --height -h +takes_value default_value("768") "Sets output image height")
        (@arg seed: --seed -s +takes_value "Sets seed for random number generator")
        (@arg output: --output -o +takes_value "PNG output file (defaults to stdout)")
    ).get_matches();

    let width: u32 = args.value_of_t_or_exit("width");
    let height: u32 = args.value_of_t_or_exit("height");

    let seed: u64 = args.value_of_t("seed")
        .unwrap_or(SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs());
    eprintln!("Using seed '{}'", seed);

    let mut rng: StdRng = SeedableRng::seed_from_u64(seed);
    let mut canvas = canvas::Canvas::new(width, height);
    canvas.design(&mut rng);

    let output: Box<dyn Write> = if args.is_present("output") {
        let path = Path::new(args.value_of("output").unwrap());
        let file = File::create(path)
            .unwrap_or_else(|e| {
                eprintln!("Failed to create output file '{}': {}", path.display(), e);
                exit(2);
            });

        Box::from(file)
    } else {
        Box::from(stdout())
    };

    eprintln!("Writing image");
    canvas.write(output);
}
