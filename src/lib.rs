mod canvas;
mod color;
mod drawable;
mod util;

#[cfg(target_os="android")]
#[allow(non_snake_case)]
pub mod android {
    extern crate jni;

    use std::time::{SystemTime, UNIX_EPOCH};

    use rand::rngs::StdRng;
    use rand::SeedableRng;

    use crate::canvas;

    use self::jni::JNIEnv;
    use self::jni::objects::JObject;
    use self::jni::sys::{jint, jintArray, jlong};

    #[no_mangle]
    pub unsafe extern fn Java_net_robiotic_ferrum_Ferrum_generateImage(env: JNIEnv, _jo: JObject, width: jint, height: jint) -> jintArray {
        let seed = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis();
        Java_net_robiotic_ferrum_Ferrum_generateImageWithSeed(env, _jo, width, height, seed as i64)
    }

    #[no_mangle]
    pub unsafe extern fn Java_net_robiotic_ferrum_Ferrum_generateImageWithSeed(env: JNIEnv, _: JObject, width: jint, height: jint, seed: jlong) -> jintArray {
        // Generate image
        let mut rng: StdRng = SeedableRng::seed_from_u64(seed as u64);
        let mut canvas = canvas::Canvas::new(width as u32, height as u32);
        canvas.design(&mut rng);

        // Pack for JNI
        let output = env.new_int_array((canvas.data.len() / 3) as i32).unwrap();

        // Convert (R, G, B, R, G, B, ...) to (RGB, RGB, ...)
        let mut image: Vec<i32> = vec![];
        image.resize(canvas.data.len() / 3, 0);

        let mut pixelIndex = 0;
        for rgb in canvas.data.chunks(3) {
            let pixel: i32 = (rgb[0] as i32) << 16 | (rgb[1] as i32) << 8 | (rgb[2] as i32);
            image[pixelIndex] = pixel;
            pixelIndex += 1;
        }

        env.set_int_array_region(output, 0, &image).unwrap();
        output
    }
}
