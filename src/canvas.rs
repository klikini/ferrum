use std::io::Write;

use rand::rngs::StdRng;

use crate::color::Color;
use crate::drawable::{Drawable, gradient::Gradient, grain::Grain};
use crate::drawable::terrain::Terrain;

pub struct Canvas {
    pub(crate) width: u32,
    pub(crate) height: u32,
    pub data: Vec<u8>,
}

impl Canvas {
    pub fn new(width: u32, height: u32) -> Canvas {
        Canvas {
            width,
            height,
            data: vec![0; (width * height * 3) as usize],
        }
    }

    pub fn design(&mut self, rng: &mut StdRng) {
        Gradient::sky(rng, self).paint(self);
        Terrain::generate(rng, self).paint(self);
        Gradient::clouds(rng, self).paint(self);
        Grain::generate(rng, self).paint(self);
    }

    /// Get the color of a pixel by its (x, y) coordinates
    pub(crate) fn get_px(&self, col: u32, line: u32) -> Color {
        let r_ptr = (line * self.width * 3 + col * 3) as usize;
        Color::from((self.data[r_ptr], self.data[r_ptr + 1], self.data[r_ptr + 2]))
    }

    /// Get the color of a pixel by its index in the data array
    pub(crate) fn get_idx(&self, index: usize) -> Color {
        let r_ptr = index * 3;
        Color::from((self.data[r_ptr], self.data[r_ptr + 1], self.data[r_ptr + 2]))
    }

    /// Set the color of a pixel by its (x, y) coordinates
    pub(crate) fn set_px(&mut self, col: u32, line: u32, new: &Color) {
        let r_ptr = (line * self.width * 3 + col * 3) as usize;
        let color = self.get_px(col, line).blend(&new);
        self.data[r_ptr] = color.r;
        self.data[r_ptr + 1] = color.g;
        self.data[r_ptr + 2] = color.b;
    }

    /// Set the color of a pixel by its index in the data array
    pub(crate) fn set_idx(&mut self, index: usize, new: &Color) {
        let r_ptr = index * 3;
        let color = self.get_idx(index).blend(&new);
        self.data[r_ptr] = color.r;
        self.data[r_ptr + 1] = color.g;
        self.data[r_ptr + 2] = color.b;
    }

    pub(crate) fn write(self, destination: Box<dyn Write>) {
        let mut png_encoder = png::Encoder::new(destination, self.width as u32, self.height as u32);
        png_encoder.set_color(png::ColorType::RGB);
        png_encoder.set_depth(png::BitDepth::Eight);

        let mut png_writer = png_encoder.write_header().unwrap();
        png_writer.write_image_data(&self.data).unwrap();
    }
}
